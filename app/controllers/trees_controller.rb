class TreesController < ApplicationController
  before_action :set_tree, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :test_user, only: [:show, :edit, :update, :destroy]

  require 'securerandom'

  def index
    if current_user.admin?
      @trees = Tree.all
    else
      @trees = Tree.where(user_id: current_user.id)
    end
    
  end

  def show
    @tree.endereco = "#{@tree.street}, #{@tree.district}, #{@tree.city} - #{@tree.state}"
  end

  def new
    @tree = Tree.new
    @tree.numbering = Faker::Number.number(10)
  end

  def edit
  end

  def create
    @tree = Tree.new(tree_params)
    continue = true
    trees = Tree.all
    @tree.user_id = current_user.id
    numbering_aux = @tree.numbering
    while continue == true
      finder = trees.where("numbering = ?", numbering_aux).count
      if finder > 0
        numbering_aux = SecureRandom.random_number 10000
      else  
        continue = false
        @tree.numbering = SecureRandom.random_number 10000
      end
    end
    @tree.numbering = numbering_aux
    if @tree.save
      redirect_to trees_path
    else
      render :new
    end
  end

  def update
    if @tree.update(tree_params)
      redirect_to trees_path
    else
      render :edit
    end
  end

  def destroy
    if @tree.destroy
      redirect_to trees_path
    end
  end

  

  private
    def set_tree
      @tree = Tree.find(params[:id])
    end

    def tree_params
      params.require(:tree).permit(:name, :user_id, :requester, :species, :date_planting, :who_planted, :old_name, :street, :city, :number, :district, :country, :state, :lat, :long, :numbering, :authorization, :date_solicitation)
    end
end
