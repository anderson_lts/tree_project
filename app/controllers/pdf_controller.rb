class PdfController < ApplicationController
  before_action :set_tree
  before_action :authenticate_user!
  before_action :test_user

  def certified
    render template: 'pdf/certified',
      handlers: [:erb],
      formats: [:pdf],
      page_size: 'A4',
      pdf: "Certificado #{l(Date.current)}",
      #layout: 'layouts/application.html',
      #header: { html: { template: 'layouts/pdf/header', handlers: [:erb], formats: [:pdf] }},
      #footer: { html: { template: 'layouts/pdf/footer', handlers: [:erb], formats: [:pdf] }},
      #margin: { bottom: 15, top: 50, right: 10, left: 10 },
      show_as_html: false,
      #disposition: 'attachment', OPÇÃO PARA BAIXAR AUTOMATICAMENTE
      encoding: 'UTF-8'
  end

  private
    def set_tree
      @tree = Tree.find(params[:id])
    end
end
