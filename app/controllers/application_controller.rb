class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  #before_action :authenticate_user!

  def test_user
    if !current_user.admin? && @tree.user_id != current_user.id
      # ADD AQUI FLASH PARA MENSAGEM DE ERRO
      redirect_to trees_path
    end
  end
  
end
