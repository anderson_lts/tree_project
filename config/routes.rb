Rails.application.routes.draw do
  get 'pdf/certified/:id'                        => 'pdf#certified',                         as: :certified
  resources :trees
  resources :trees
	devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }

	authenticated :user do
		root 'home#index', as: 'authenticated_root'
	end
	devise_scope :user do
		root 'devise/sessions#new'
	end
end
