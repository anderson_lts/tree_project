# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181023215023) do

  create_table "trees", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.string   "requester"
    t.string   "species"
    t.date     "date_planting"
    t.string   "who_planted"
    t.string   "old_name"
    t.string   "state"
    t.string   "city"
    t.string   "country"
    t.string   "number"
    t.string   "district"
    t.string   "street"
    t.string   "lat"
    t.string   "long"
    t.string   "numbering"
    t.string   "authorization"
    t.date     "date_solicitation"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "trees", ["user_id"], name: "index_trees_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "name",                   default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "admin",                  default: false
    t.string   "provider"
    t.string   "uid"
    t.boolean  "is_facebook",            default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
