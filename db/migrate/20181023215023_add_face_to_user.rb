class AddFaceToUser < ActiveRecord::Migration
  def change
    add_column :users, :is_facebook, :boolean, default: false
  end
end
