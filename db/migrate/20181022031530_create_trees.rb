class CreateTrees < ActiveRecord::Migration
  def change
    create_table :trees do |t|
      t.string :name
      t.references :user, index: true, foreign_key: true
      t.string :requester
      t.string :species
      t.date :date_planting
      t.string :who_planted
      t.string :old_name
      t.string :state
      t.string :city
      t.string :country
      t.string :number
      t.string :district
      t.string :street
      t.string :lat
      t.string :long
      t.string :numbering
      t.string :authorization
      t.date :date_solicitation

      t.timestamps null: false
    end
  end
end
